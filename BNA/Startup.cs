﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BNA.Startup))]
namespace BNA
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
