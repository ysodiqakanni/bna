﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BNA.Core
{
    public class Entity
    {
        public int ID { get; set; }
        public string CreatedBy { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}