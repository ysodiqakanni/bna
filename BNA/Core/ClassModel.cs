﻿namespace BNA.Core
{
    public class ClassModel
    {
        public int NumberOfStudentsInClass { get; set; }
        public virtual Class Class { get; set; }
    }
}